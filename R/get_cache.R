import_ci_drake_cache <- function(tag = "master", gitlab_pat = Sys.getenv("GITLAB_PAT"),
                                  cache_zipfile = "drake_cache.zip",
                                  project = "14475381",
                                  job = "run-drake") {
  td <- tempfile()
  dir.create(td)
  zippath <- file.path(td, cache_zipfile)
  cache_zip <- httr::GET(
    paste0("https://gitlab.com/api/v4/projects/", project, "/jobs/artifacts/", tag, "/raw/artifacts/", "drake_cache.zip?job=", job),
    httr::add_headers(`PRIVATE-TOKEN` = Sys.getenv("GITLAB_PAT")),
    httr::config(followlocation = 1),
    httr::write_disk(zippath, overwrite = TRUE)
  )
  zip::unzip(zippath, exdir = file.path(td))
  if (!dir.exists(".drake")) drake::new_cache()
  my_cache <- drake::drake_cache(path = ".drake")
  ci_cache <- drake::drake_cache(path = file.path(td, ".drake"))
  my_cache$import(ci_cache)
  my_cache$history$import(ci_cache$history)
}